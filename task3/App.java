import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[][] matrix;
        int rows = 0, cols = 0;

        try {
            System.out.println("Please enter number of rows in matrix (positive integer):");
            rows = scanner.nextInt();
            System.out.println("Please enter number of columns in matrix (positive integer):");
            cols = scanner.nextInt();
            scanner.close();
        } catch (InputMismatchException e) {
            System.out.println("Error! Input is invalid.");
            System.exit(-1);
        }

        if(rows < 1 || cols < 1){
            System.out.println("Error! Input is invalid.");
            System.exit(1);
        }

        matrix = new int[rows][cols];
        ArrayList <int[]> intRoots = new ArrayList<>();
        int value = 0, rootsSum = 0;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                matrix[i][j] = value;
                double root = Math.sqrt(matrix[i][j]);
                if(isInteger(root) == true){
                    intRoots.add(new int[]{i, j, value, (int)root});
                }
                value++;
            }
        }

        System.out.println("Initial matrix:");
        System.out.println(Arrays.deepToString(matrix));

        System.out.println("List of integer roots:");
        for (int[] note: intRoots) {
            System.out.print("Indexes: ["+note[0]+","+note[1]+"] ");
            System.out.println("Value = " + note[2]+ " Root = " + note[3]);
            rootsSum += note[3];
        }

        System.out.println("Roots in reverse order:");
        for (int i = intRoots.size()-1; i >= 0; i--) {
            System.out.print(intRoots.get(i)[3]+" ");
        }

        System.out.println("");
        System.out.println("Sum of roots = " + rootsSum);

    }
    public static boolean isInteger(double number){
        if(number%1 == 0) return true;
        return false;
    }
    

}
