import java.util.InputMismatchException;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int inputValue = -1;

        System.out.println("Please enter positive integer number:");
        try {
            inputValue = scanner.nextInt();
            scanner.nextLine();
            if(inputValue > 0) {
                if(inputValue >= 10){
                    System.out.println("Warning! An operation is time-consuming.");
                    System.out.println("Do you confirm the operation? (Enter \"Yes\" to confirm):");
                    String answer = scanner.nextLine();
                    if(answer.equals("Yes")){
                        System.out.println(inputValue+"! = "+ calculateFactorial(inputValue));
                    } else {
                        System.exit(0);
                    }
                } else {
                    System.out.println(inputValue+"! = "+ calculateFactorial(inputValue));
                }
            } else {
                System.out.println("Error! Input is invalid.");
                System.exit(1);
            }
            scanner.close();
        } catch (InputMismatchException e) {
            System.out.println("Error! Input is invalid.");
            System.exit(-1);
        }

    }

    public static long calculateFactorial(long num) {
        if (num > 1) {
            return num * calculateFactorial(num - 1);
        } else {
            return 1;
        }
    }
}


