import java.util.InputMismatchException;
import java.util.Scanner;

public class App {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        int bound = -1;

        System.out.println("Please enter positive integer number:");

        try {
            bound = scanner.nextInt();
            if(bound > 0) {
                printPrimeNumbersLessThan(bound);
            } else {
                System.out.println("Error! Input is invalid.");
                System.exit(1);
            }
            scanner.close();
        } catch (InputMismatchException e) {
            System.out.println("Error! Input is invalid.");
            System.exit(-1);
        }

    }
    public static void printPrimeNumbersLessThan(int bound){
        System.out.println("Prime numbers less than"+ bound +"are: ...");
        for (int checkingNumber = bound-1; checkingNumber >= 0; checkingNumber--) {
            boolean isPrime = true;
            for (int j = 2; j < checkingNumber; j++) {
                if(checkingNumber%j == 0){
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                System.out.print(checkingNumber + " ");
            }
        }
    }

}
